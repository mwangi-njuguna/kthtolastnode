var LinkedList = require('../solution');
var assert = require('assert');

describe('LinkedList implementation', () => {
  const theLinkedList = new LinkedList(null);

  theLinkedList.add('Angel Food');
  theLinkedList.add('Bundt');
  theLinkedList.add('Cheese');
  theLinkedList.add("Devil's Food");
  theLinkedList.add('Eccles');

  describe('kthToLastNode method', () => {
    it('should return correct node', () => {
      const resNode = theLinkedList.kthToLastNode(2, theLinkedList.head);
      assert(resNode.element, "Devil's Food");
    });

    it('should return correct node', () => {
      const resNode = theLinkedList.kthToLastNode(1, theLinkedList.head);
      assert(resNode.element, 'Eccles');
    });

    it('should throw an error if k is out of bounds', () => {
      assert.throws(() => theLinkedList.kthToLastNode(5, theLinkedList.head));
    });

    it('should throw an error if k is zero', () => {
      assert.throws(
        () => theLinkedList.kthToLastNode(0, theLinkedList.head),
        Error
      );
    });

    it('should throw an error if linkedlist is empty', () => {
      const mockList = new LinkedList(null);
      assert.throws(() => mockList.kthToLastNode(0, mockList.head));
    });

    it('should throw an error if k is a negative number', () => {
      assert.throws(
        () => theLinkedList.kthToLastNode(-78, theLinkedList.head),
        Error
      );
    });
  });
});
