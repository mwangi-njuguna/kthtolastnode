//create a blueprint(shape) of the node
class Node {
  constructor(elem) {
    this.element = elem;
    this.next = null;
  }
}

//implement the linkedlist data stucture
class LinkedList {
  constructor(elem) {
    this.head = elem;
    this.size = 0;
  }

  //add element to end of list
  add(elem) {
    //create a node
    const newNode = new Node(elem);

    if (this.head === null) {
      this.head = newNode;
    }
    // iterate till the tail and append the new node
    else {
      let current = this.head;

      //iterate till the end(tail)
      while (current.next !== null) {
        current = current.next;
      }

      //add the new node after the last node
      current.next = newNode;
    }

    //increment the size of the linkedlist
    this.size += 1;
  }

  size() {
    return this.size;
  }

  clear() {
    //help needed
    //has idea but time complexity is very high
  }

  kthToLastNode(/* Integer */ k, /* Node */ headNode) {
    //get the nodes to skip
    // this makes the worst-case scenario O(N)
    let nodeCounter = this.size - k;

    // if k is negative or the linkedlist is empty then throw an error or k is not an integer
    if (
      this.size === 0 ||
      nodeCounter === this.size ||
      nodeCounter < k ||
      k % 1 !== 0
    ) {
      throw new Error('Not supported');
    }

    let current = headNode;

    while (nodeCounter > 0) {
      current = current.next;
      nodeCounter -= 1;
    }

    return current;
  }
}

module.exports = LinkedList;
